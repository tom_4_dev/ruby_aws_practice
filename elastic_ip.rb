require "./aws_base"

class ElasticIp < AwsBase
  def initialize
    @ec2 = Aws::EC2::Client.new(region: secret_region_name, credentials: credentials)
  end

  def display_addresses
    ec2 = @ec2
    describe_addresses_result = ec2.describe_addresses
    describe_addresses_result.addresses.each do |address|
      puts "=" * 10
      puts "Allocation ID: #{address.allocation_id}"
      puts "Association ID: #{address.association_id}"
      puts "Instance ID: #{address.instance_id}"
      puts "Public IP: #{address.public_ip}"
      puts "Private IP Address: #{address.private_ip_address}"
    end
  end

  def allocate_addresses
    @ec2.allocate_address({ domain: "vpc"})
  end

  def associate_address
    puts "enter allocation id"
    allocation_id = gets
    allocation_id = allocation_id.chomp

    puts "enter instance id"
    instance_id = gets
    instance_id = instance_id.chomp

    @ec2.associate_address({
                            allocation_id: allocation_id,
                            instance_id: instance_id,
                          })
  end

  def release_address(allocation_id)
    puts "\nReleasing the address from the instance..."
    @ec2.release_address(allocation_id: allocation_id)
  end
end

# ec2 = ElasticIp.new
# puts "assume elastic ip is there allocated"
# puts "\nAfter allocating the address for instance, but before associating the address with the instance..."
# ec2.display_addresses
