require 'json'
require 'aws-sdk'
require 'pry'

class AwsBase

  private

  def credentials
    creds = JSON.load(File.read('secret.json'))

    Aws::Credentials.new(creds["access_key_id"], creds["secret_key"])
  end

  def secret_region_name
    creds = JSON.load(File.read('secret.json'))
    creds["region_name"]
  end
end
