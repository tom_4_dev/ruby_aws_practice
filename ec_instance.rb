require '/.aws_base'

class EcInstance < AwsBase
  attr_reader :name

  def initialize
    @ec2 = Aws::EC2::Resource.new(
      region: secret_region_name,
      credentials: credentials,
    )
  end

  def create_instance
    puts "Enter ami id from where the instance will be created:"
    ami_id = gets
    ami_id = ami_id.chomp
    puts "Enter volume size in gb:"
    volume = gets
    volume = volume.chomp
    puts "The instance type is t3a.nano. If you want to change change the instance type"
    sleep 2
    puts "Enter key name:"
    key_name = gets
    key_name = key_name.chomp
    puts "Do you want to enable termination protection? y/n"
    protection = gets
    protection = protection.chomp

    options = {
      block_device_mappings: [
        {
          device_name: "/dev/sda1",
          #virtual_name: "test_device_virtual_name",
          ebs: {
            delete_on_termination: true, # Indicates whether the EBS volume is deleted on instance termination.
            # iops: 1, # The number of I/O operations per second (IOPS) that the volume supports NB: not supported with gp2
            volume_size: volume.to_i,
            volume_type: "gp2", # accepts standard, io1, gp2, sc1, st1
            encrypted: false,
          }
        },
      ],
      image_id: ami_id,
      instance_type: "t3a.nano",
      key_name: key_name,
      max_count: 1, # required
      min_count: 1, # required
      monitoring: {
        enabled: false, # required
      },
      disable_api_termination: protection.downcase == "y",
      dry_run: false,
      ebs_optimized: false,
      instance_initiated_shutdown_behavior: "stop", # accepts stop, terminate
      credit_specification: {
        cpu_credits: "standard", # required standard/ unlimited
      },
      hibernation_options: {
        configured: false,
      },
    }

    @ec2.create_instances(options)
    puts "created successfully..."
  end

  def list_of_instances
    @ec2.instances.each do |i|
      puts i.id
    end
  end

  def disable_machine_termination
    puts "What is id of the machine?"
    id = gets
    id = id.chomp
    i = @ec2.instance(id)
    i.modify_attribute(disable_api_termination: { value: false })
    puts "Instance can be terminated now"
  end

  def delete_instance
    puts "What is id of the instance to terminate?"
    id = gets
    id = id.chomp
    i = @ec2.instance(id)

    if i.exists?
      case i.state.code
      when 48  # terminated
        puts "#{id} is already terminated"
      else
        i.terminate
        puts "terminated succssfully"
      end
    end
  end

end

=begin
  ec = EcInstance.new
  # create an ec2 instance
  ec.create_instance
  # list of instances
  ec.list_of_instances
  # disable termination protection
  ec.disable_machine_termination
  # terminate an ec2 instance
  ec.delete_instance
=end
